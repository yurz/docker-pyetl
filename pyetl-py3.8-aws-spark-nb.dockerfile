FROM yurz/pyetl:py3.8-aws

LABEL authors="Yuri Zhylyuk <yuri@zhylyuk.com>"

ENV DEBIAN_FRONTEND noninteractive


######################################################
######################################################
USER root

# Spark dependencies
# Default values can be overridden at build time
# (ARGS are in lower case to distinguish them from ENV)
ARG spark_version="3.0.1"
ARG hadoop_version="3.2"
ARG spark_checksum="E8B47C5B658E0FBC1E57EEA06262649D8418AE2B2765E44DA53AAF50094877D17297CC5F0B9B35DF2CEEF830F19AA31D7E56EAD950BBE7F8830D6874F88CFC3C"
ARG openjdk_version="11"

ENV APACHE_SPARK_VERSION="${spark_version}" \
    HADOOP_VERSION="${hadoop_version}"

RUN apt-get -y update && \
    apt-get install --no-install-recommends -y \
    "openjdk-${openjdk_version}-jre-headless" \
    ca-certificates-java && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Spark installation
WORKDIR /tmp
# Using the preferred mirror to download Spark
# hadolint ignore=SC2046
RUN wget -q $(wget -qO- https://www.apache.org/dyn/closer.lua/spark/spark-${APACHE_SPARK_VERSION}/spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz\?as_json | \
    python -c "import sys, json; content=json.load(sys.stdin); print(content['preferred']+content['path_info'])") && \
    echo "${spark_checksum} *spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz" | sha512sum -c - && \
    tar xzf "spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz" -C /usr/local --owner root --group root --no-same-owner && \
    rm "spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz"

WORKDIR /usr/local

# Configure Spark
ENV SPARK_HOME=/usr/local/spark
ENV PATH=$PATH:$SPARK_HOME/bin
# ENV SPARK_OPTS="--driver-java-options=-Xms1024M --driver-java-options=-Xmx4096M --driver-java-options=-Dlog4j.logLevel=info" 

RUN ln -s "spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}" spark && \
    # Add a link in the before_notebook hook in order to source automatically PYTHONPATH
    mkdir -p /usr/local/bin/before-notebook.d && \
    ln -s "${SPARK_HOME}/sbin/spark-config.sh" /usr/local/bin/before-notebook.d/spark-config.sh
######################################################
######################################################


RUN \
    apt-get -y update \
    && apt-get -y --no-install-recommends upgrade \
    ##
    #### START: jupyter specific modules and setup
    && pip install --no-cache-dir black blackcellmagic nb_black jupyter jupyterthemes jupyterlab \
      plotly \
    ## + spark and dask related:
      pyarrow pyspark seaborn dask xgboost sklearn faker \
    && mkdir -p ~/.ipython/profile_default/startup \
    && echo "get_ipython().run_line_magic('load_ext', 'autoreload')" >> ~/.ipython/profile_default/startup/load_extensions.py \
    && echo "get_ipython().run_line_magic('load_ext', 'nb_black')" >> ~/.ipython/profile_default/startup/load_extensions.py \
    #### END: jupyter specific modules and setup
    ##
    #### START: modules for vscode in contaner development:
    && pip install --no-cache-dir flake8 flake8-black flake8-docstrings \
    #### END: modules for vscode in contaner development
    ##
    && conda clean --all --force-pkgs-dirs --yes \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/{cache,log}/

COPY entrypoint-nb.sh /entrypoint.sh
RUN \
    chmod +x /entrypoint.sh 

RUN addgroup --gid 1000 container_user
RUN adduser --disabled-password --gecos 'container user' --uid 1000 --gid 1000 container_user
USER container_user

WORKDIR /app
CMD ["/entrypoint.sh"]


# docker build --squash -t yurz/pyetl:py3.8-aws-spark-nb -f pyetl-py3.8-aws-spark-nb.dockerfile .
