FROM yurz/pyetl:py3.9-ta

LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ENV DEBIAN_FRONTEND noninteractive

RUN \
    apt-get -y update \
    && apt-get -y --no-install-recommends upgrade \
    ##
    #### START: jupyter specific modules and setup
    && pip install --no-cache-dir black blackcellmagic nb_black jupyter jupyterthemes jupyterlab \
    && mkdir -p ~/.ipython/profile_default/startup \
    && echo "get_ipython().run_line_magic('load_ext', 'autoreload')" >> ~/.ipython/profile_default/startup/load_extensions.py \
    && echo "get_ipython().run_line_magic('load_ext', 'nb_black')" >> ~/.ipython/profile_default/startup/load_extensions.py \
    #### END: jupyter specific modules and setup
    ##
    #### START: modules for vscode in contaner development:
    && pip install --no-cache-dir flake8 flake8-black flake8-docstrings \
    #### END: modules for vscode in contaner development
    ##
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/{cache,log}/

COPY entrypoint-nb.sh /entrypoint.sh
RUN \
    chmod +x /entrypoint.sh 

RUN addgroup --gid 1000 container_user
RUN adduser --disabled-password --gecos 'container user' --uid 1000 --gid 1000 container_user
USER container_user

WORKDIR /app
CMD ["/entrypoint.sh"]


# docker build --network=host --no-cache --squash -t yurz/pyetl:py3.9-ta-nb -f pyetl-py3.9-ta-nb.dockerfile .
