#!/bin/bash

# Jupyter Notebook version

export PATH="$HOME/.local/bin:$PATH"
export PYTHONPATH="$HOME/.local/lib/python3.9/site-packages:$PYTHONPATH"

cd /app

# install any adhoc python libraries if required:
# script will install addtional Python libs from PY_REQ_FILE if provided
# if not - will also check if there is requirements.txt file and try installing from it
if [ -z "$PY_REQ_FILE" ]; then
    echo "no PY_REQ_FILE provided"
    if test -f "requirements.txt"; then
        echo "requirements.txt found"
        echo "installing additional python libraries from requirements.txt"
        pip install --no-cache-dir --requirement requirements.txt
    else
        echo "no default requirements.txt file found"
    fi
else
    echo "installing additional python libraries from $PY_REQ_FILE"
    pip install --no-cache-dir --requirement $PY_REQ_FILE
fi

jt -t onedork -fs 12 -altp -tfs 11 -nfs 115 -cellw 90% -T -N
echo "starting jupyter"
jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root --NotebookApp.token=''
