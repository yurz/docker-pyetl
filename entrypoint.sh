#!/bin/bash

export PATH="$HOME/.local/bin:$PATH"
export PYTHONPATH="$HOME/.local/lib/python3.9/site-packages:$PYTHONPATH"

# get app files from S3
# S3_FILES can accept either s3 path as a directory
# or comma separated list of s3 file locations, e.g.:
#    "s3://my_bucket/my_directory"
#    "s3://my_bucket/my_directory/py_file.py,s3://my_bucket/my_directory/py_file2.py,s3://my_bucket/my_directory/requirements.txt,"
# if a single s3 file location is being provided it has to end with a comma to indicate that it's an individual file and not a directory, e.g.:
#    "s3://my_bucket/my_directory/py_file.py,"
if [ -z "$S3_FILES" ]; then
    echo "no S3_FILES env provided"
else
    if [[ $S3_FILES == *,* ]]; then
        echo "loading individual files:"
        IFS=',' read -ra FILES <<<"$S3_FILES"
        for F in "${FILES[@]}"; do
            F=$F | sed 's/^[[:blank:]]*//;s/[[:blank:]]*$//'
            aws s3 cp $F /app
            echo $F
        done
    else
        echo "loading all files from $S3_FILES:"
        aws s3 cp $S3_FILES /app --recursive --exclude "*.log"
    fi
fi

cd /app

# install any adhoc python libraries if required:
# script will install addtional Python libs from PY_REQ_FILE if provided
# if not - will also check if there is requirements.txt file and try installing from it
if [ -z "$PY_REQ_FILE" ]; then
    echo "no PY_REQ_FILE provided"
    if test -f "requirements.txt"; then
        echo "requirements.txt found"
        echo "installing additional python libraries from requirements.txt"
        pip install --no-cache-dir --requirement requirements.txt
    else
        echo "no default requirements.txt file found"
    fi
else
    echo "installing additional python libraries from $PY_REQ_FILE"
    pip install --no-cache-dir --requirement $PY_REQ_FILE
fi

# app entry command to execute on docker run
# can be python or bash script or any executable
# for example:
#     "python my_script.py"
#     "bash my_script.sh"
# "python app.py" executed by default
eval "${APP_ENTRY:-python app.py}"
