#!/bin/bash

docker build --network=host --squash -t yurz/pyetl:py3.9-ta -f pyetl-py3.9-ta.dockerfile . \
&& \
docker build --network=host --squash -t yurz/pyetl:py3.9-ta-nb -f pyetl-py3.9-ta-nb.dockerfile .
