FROM ubuntu:20.04

LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ENV DEBIAN_FRONTEND=noninteractive

RUN \
    apt-get -y update \
    && apt-get -y --no-install-recommends upgrade \
    && apt-get -y --no-install-recommends install apt-utils \
        wget nano tzdata git libpq-dev bzip2 build-essential zlib1g-dev software-properties-common \
    && apt-get -y --no-install-recommends install --reinstall ca-certificates \
    && update-ca-certificates  \
    && add-apt-repository ppa:deadsnakes/ppa \
    && apt-get -y update \
    && apt-get -y install python3.9 python3.9-dev python3-pip \
    && rm -f /usr/bin/python3 \
    && ln -s /usr/bin/python3.9 /usr/bin/python3 \
    && ln -s /usr/bin/python3 /usr/bin/python \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/{cache,log}/

# docker build --network=host --no-cache --squash -t yurz/pybase:3.9 -f pybase-3.9.dockerfile .
