FROM yurz/pybase:3.8

LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ENV DEBIAN_FRONTEND noninteractive

RUN \
    apt-get -y update \
    && apt-get -y --no-install-recommends upgrade \
    && pip install --no-cache-dir --prefer-binary fsspec s3fs awscli boto3 awswrangler \
       httpx requests httplib2 aiohttp lxml pytz SQLAlchemy psycopg2-binary \
       xlrd openpyxl xlsxwriter joblib pandas geopandas \
       pysftp google-api-python-client oauth2client plotly \
    && conda clean --all --force-pkgs-dirs --yes \
    && find /miniconda/ -follow -type f -name '*.a' -delete \
    && find /miniconda/ -follow -type f -name '*.pyc' -delete \
    && find /miniconda/ -follow -type f -name '*.js.map' -delete \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/{cache,log}/

COPY entrypoint.sh /entrypoint.sh
RUN \
    chmod +x /entrypoint.sh 

WORKDIR /app
CMD ["/entrypoint.sh"]


# docker build --squash -t yurz/pyetl:py3.8-aws -f pyetl-py3.8-aws.dockerfile .
