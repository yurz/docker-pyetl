FROM ubuntu:20.04

LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ENV DEBIAN_FRONTEND=noninteractive

RUN \
    apt-get -y update \
    && apt-get -y --no-install-recommends upgrade \
    && apt-get -y --no-install-recommends install apt-utils \
        wget nano libpq-dev bzip2 tzdata git \ 
    && apt-get -y --no-install-recommends install --reinstall ca-certificates \
    && update-ca-certificates \
    && cd ~ \
    && wget https://repo.anaconda.com/miniconda/Miniconda3-py38_4.8.3-Linux-x86_64.sh \
    && chmod +x ~/Miniconda3-py38_4.8.3-Linux-x86_64.sh \
    && ~/Miniconda3-py38_4.8.3-Linux-x86_64.sh -p /miniconda -b \
    && rm ~/Miniconda3-py38_4.8.3-Linux-x86_64.sh \
    && PATH=/miniconda/bin:$PATH \
    && conda clean --all --force-pkgs-dirs --yes \
    && find /miniconda/ -follow -type f -name '*.a' -delete \
    && find /miniconda/ -follow -type f -name '*.pyc' -delete \
    && find /miniconda/ -follow -type f -name '*.js.map' -delete \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/{cache,log}/
ENV PATH /miniconda/bin:$PATH


# docker build --squash -t yurz/pybase:3.8 -f pybase-3.8.dockerfile .
