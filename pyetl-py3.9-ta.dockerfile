FROM yurz/pybase:3.9

LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ENV DEBIAN_FRONTEND noninteractive

RUN \
    apt-get -y update \
    && apt-get -y --no-install-recommends upgrade \
    && pip install --no-cache-dir --prefer-binary \
      httpx requests httplib2 aiohttp lxml pytz SQLAlchemy psycopg2-binary \
      xlrd openpyxl xlsxwriter joblib numpy==1.20 pandas geopandas joblib pyarrow matplotlib \
      seaborn scikit-learn imblearn scipy xgboost pysftp oauth2client plotly TA-Lib \
      duckdb datefinder metaflow \
      auto-sklearn flaml evalml tpot pycaret lightautoml autogluon.tabular[all] \
      mljar-supervised \
    && wget http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz \
    && tar xzvf ta-lib-0.4.0-src.tar.gz \
    && cd ta-lib \
    && ./configure --prefix=/usr \
    && make && make install \
    && cd .. \
    && rm -rf ta-lib* \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/{cache,log}/

COPY entrypoint-local.sh /entrypoint.sh
RUN \
    chmod +x /entrypoint.sh 

WORKDIR /app
CMD ["/entrypoint.sh"]

# docker build --network=host --no-cache --squash -t yurz/pyetl:py3.9-ta -f pyetl-py3.9-ta.dockerfile .
