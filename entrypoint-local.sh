#!/bin/bash

export PATH="$HOME/.local/bin:$PATH"
export PYTHONPATH="$HOME/.local/lib/python3.9/site-packages:$PYTHONPATH"

cd /app

# install any adhoc python libraries if required:
# script will install addtional Python libs from PY_REQ_FILE if provided
# if not - will also check if there is requirements.txt file and try installing from it
if [ -z "$PY_REQ_FILE" ]; then
    echo "no PY_REQ_FILE provided"
    if test -f "requirements.txt"; then
        echo "requirements.txt found"
        echo "installing additional python libraries from requirements.txt"
        pip install --no-cache-dir --requirement requirements.txt
    else
        echo "no default requirements.txt file found"
    fi
else
    echo "installing additional python libraries from $PY_REQ_FILE"
    pip install --no-cache-dir --requirement $PY_REQ_FILE
fi

# app entry command to execute on docker run
# can be python or bash script or any executable
# for example:
#     "python my_script.py"
#     "bash my_script.sh"
# "python app.py" executed by default
eval "${APP_ENTRY:-python app.py}"
