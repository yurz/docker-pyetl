docker run -it --rm \
    -p 8888:8888 \
    --net='host' \
    --dns-search='local' \
    -v "$(pwd)"/app:/app \
    -v ~/.aws:/root/.aws \
    -e APP_ENTRY="python my_script_sample.py" \
    yurz/pyetl:py3.8-aws-ta

# -e S3_FILES="s3://my_bucket/my_folder/app1.py,s3://my_bucket/my_folder/my_requirements.txt" \
# -e PY_REQ_FILE="my_requirements.txt" \
